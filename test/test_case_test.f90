! Unit tests of test_case

! Copyright 2018 Steve Biggs

! This file is part of fortunit.

! fortunit is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! fortunit is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with fortunit.  If not, see http://www.gnu.org/licenses/.
module test_case_test_mod

    use fortunit, only: test_case
    use was_run_test_procedure_to_be_run_mod

    implicit none

    type, abstract, extends(test_case) :: test_case_test
    contains
        procedure :: test_was_run
    end type test_case_test


contains

    subroutine test_was_run(self)
        class(test_case_test) :: self
        type(was_run_test_procedure_to_be_run) :: test
        print *, test%was_run
        call test%run()
        print *, test%was_run
    end subroutine test_was_run


end module test_case_test_mod
