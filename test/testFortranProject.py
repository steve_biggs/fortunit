"""
Unit tests of ModuleFinder

Copyright 2018 Stephen Biggs-Fox

This file is part of fortunit.

fortunit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fortunit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with fortunit.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase, mock
import sys
sys.path.insert(0, '../src')
from FortranProject import FortranProject


class testFortranProjectFortranModuleFinding(TestCase):

    def setUp(self):
        # Mock directory structure
        # /path/to/project_dir/
        # - a_fortran_module.f90
        # - another_fortran_module.f90
        # - not_a_fortran_module.txt
        # - subdir/
        # -- fortran_module_in_subdir.f90
        # -- not_a_fortran_module_in_subdir.txt
        self.sep = '/'  # Don't use os.sep to avoid unnecessary dependency
        self.folder = self.sep + 'path' + self.sep + 'to' + \
            self.sep + 'project_dir'
        self.subdir = 'subdir'
        self.file0 = 'a_fortran_module.f90'
        self.file1 = 'another_fortran_module.f90'
        self.file2 = 'not_a_fortran_module.txt'
        self.file3 = 'fortran_module_in_subdir.f90'
        self.file4 = 'not_a_fortran_module_in_subdir.txt'
        self.patcher = mock.patch('os.walk')
        self.mockWalk = self.patcher.start()
        self.mockWalk.return_value = [
            (self.folder,
             (self.subdir),
             (self.file0, self.file1, self.file2)),
            (self.folder + self.sep + self.subdir,
             (),
             (self.file3, self.file4)),
        ]
        self.project = FortranProject(self.folder)

    def tearDown(self):
        self.patcher.stop()

    def testMatchingFileIsFound(self):
        modules = self.project.findModules()
        self.assertIn(self.folder + self.sep + self.file0, modules)

    def testAnotherMatchingFileIsFound(self):
        modules = self.project.findModules()
        self.assertIn(self.folder + self.sep + self.file1, modules)

    def testNonMatchingFileIsNotFound(self):
        modules = self.project.findModules()
        self.assertNotIn(self.folder + self.sep + self.file2, modules)

    def testMatchingFileInSubdirIsFound(self):
        modules = self.project.findModules()
        self.assertIn(self.folder + self.sep + self.subdir +
                      self.sep + self.file3, modules)

    def testNonMatchingFileInSubdirIsNotFound(self):
        modules = self.project.findModules()
        self.assertNotIn(self.folder + self.sep + self.subdir +
                         self.sep + self.file4, modules)
