"""
This class finds fortunit test modules from a list of fortran files

Copyright 2018 Stephen Biggs-Fox

This file is part of fortunit.

fortunit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fortunit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with fortunit.  If not, see http://www.gnu.org/licenses/.
"""

import re


class FortunitModuleFinder:

    def __init__(self, files):
        self.files = files

    def find(self):
        files = []
        for filename in self.files:
            with open(filename, 'r') as f:
                for line in f.readlines():
                    if re.search('use fortunit', line):
                        files.append(filename)
                        break
        return files
