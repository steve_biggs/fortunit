"""
Finds Fortran module files that might contain tests

Copyright 2018 Stephen Biggs-Fox

This file is part of fortunit.

fortunit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fortunit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with fortunit.  If not, see http://www.gnu.org/licenses/.
"""

import os


class FortranProject:

    def __init__(self, folder):
        self.folder = folder

    def findModules(self):
        return [os.path.join(dirs, f) for dirs, subdirs, files in
                os.walk(self.folder) for f in files if
                f.endswith('.f90')]
